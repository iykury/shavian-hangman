#!/usr/bin/env python3
import discord, os, random
import config
import game

#Make sure the current working directory is the correct one
file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)
os.chdir(folder)

#All Shavian letters
ALPHA = "𐑐𐑑𐑒𐑓𐑔𐑕𐑖𐑗𐑘𐑙𐑚𐑛𐑜𐑝𐑞𐑟𐑠𐑡𐑢𐑣𐑤𐑥𐑦𐑧𐑨𐑩𐑪𐑫𐑬𐑭𐑮𐑯𐑰𐑱𐑲𐑳𐑴𐑵𐑶𐑷𐑸𐑹𐑺𐑻𐑼𐑽𐑾𐑿"

intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

wordlist = []

#Get word list
print("Reading word list...", end="", flush=True)
with open("kingsleyreadlexicon.tsv") as f:
	for line in f.readlines():
		if line == "\n":
			continue
		word = line.split("\t")[1]
		#The Lexicon should be sorted, so only the last few need to be checked for duplicates, which speeds it up a ton
		if not word in wordlist[-10:]:
			wordlist.append(word)
print("done")

channel = None
mod_role = None

@client.event
async def on_ready():
	global channel, mod_role
	print("Logged in as")
	print(client.user.name)
	print(client.user.id)
	print("------")

	#Get hangman channel from id
	if channel is None:
		channel = client.get_channel(config.channel_id)
		if channel is None:
			print("Couldn't get hangman channel")

	#Get moderator role from id
	if mod_role is None:
		mod_role = client.guilds[0].get_role(config.mod_role_id)
		if mod_role is None:
			print("Couldn't get moderator role")
	

@client.event
async def on_message(message):
	#Bot won't reply to itself
	if message.author == client.user:
		return

	parts = message.content.split(" ")
	command = None
	if parts[0].startswith("!"):
		command = parts[0][1:]
	
	#Set Hangman channel
	if command in ["𐑣𐑨𐑙𐑥𐑨𐑯", "hangman"]:
		global channel 
		if not mod_role in message.author.roles:
			await message.channel.send("𐑴𐑯𐑤𐑦 𐑥𐑪𐑛𐑼𐑱𐑑𐑼𐑟 𐑒𐑨𐑯 𐑕𐑧𐑑 𐑢𐑦𐑗 𐑗𐑨𐑯𐑩𐑤 𐑲𐑥 𐑦𐑯.")
			return

		last_channel = message.channel
		if not channel is None:
			last_channel = channel
		await last_channel.send(f"𐑣𐑨𐑙𐑥𐑨𐑯 𐑦𐑟 𐑯𐑬 𐑦𐑯 <#{message.channel.id}>.")

		channel = message.channel
		config.channel_id = message.channel.id
		with open("channel", "w") as f:
			f.write(str(config.channel_id))
	
	if message.channel == channel:
		if command in ["𐑕𐑑𐑸𐑑", "start"]:
			if game.status["playing"]:
				await channel.send("𐑩 𐑜𐑱𐑥 𐑦𐑟 𐑷𐑤𐑮𐑧𐑛𐑦 𐑦𐑯 𐑐𐑮𐑪𐑜𐑮𐑧𐑕. 𐑑𐑲𐑐 \"!𐑕𐑑𐑪𐑐\" 𐑑 𐑧𐑯𐑛 𐑦𐑑.")
			else:
				game.status["playing"] = True
				game.status["word"] = random.choice(wordlist)
				await game.send(channel)

		if command in ["𐑕𐑑𐑪𐑐", "𐑧𐑯𐑛", "stop", "end"]:
			if game.status["playing"]:
				await game.send(channel, end=True)
			else:
				await channel.send("𐑞𐑺 𐑦𐑟 𐑯𐑴 𐑜𐑱𐑥 𐑒𐑳𐑮𐑩𐑯𐑑𐑤𐑦 𐑦𐑯 𐑐𐑮𐑪𐑜𐑮𐑧𐑕. 𐑑𐑲𐑐 \"!𐑕𐑑𐑸𐑑\" 𐑑 𐑕𐑑𐑸𐑑 𐑢𐑳𐑯.")

		if command in ["𐑣𐑧𐑤𐑐", "help"]:
			await channel.send(
				"𐑣𐑨𐑙𐑥𐑨𐑯 𐑣𐑧𐑤𐑐:\n" +
				f"𐑑 𐑥𐑱𐑒 𐑩 𐑜𐑧𐑕, 𐑑𐑲𐑐 𐑩 𐑕𐑦𐑙𐑜𐑩𐑤 ·𐑖𐑱𐑝𐑾𐑯 𐑤𐑧𐑑𐑼 𐑦𐑯 𐑩 𐑥𐑧𐑕𐑦𐑡 𐑦𐑯 𐑞𐑦𐑕 𐑗𐑨𐑯𐑩𐑤.\n" +
				"𐑿 𐑒𐑨𐑯 𐑷𐑤𐑕𐑴 𐑜𐑧𐑕 𐑞 𐑣𐑴𐑤 𐑢𐑻𐑛 𐑨𐑑 𐑢𐑳𐑯𐑕 𐑢𐑦𐑔 𐑞 \"!𐑜𐑧𐑕\" 𐑒𐑩𐑥𐑨𐑯𐑛.\n" +
				"\n" +
				"𐑣𐑨𐑙𐑥𐑨𐑯 𐑒𐑩𐑥𐑨𐑯𐑛𐑟:\n" +
				"**!𐑕𐑑𐑸𐑑**, **!start**: 𐑕𐑑𐑸𐑑𐑕 𐑩 𐑯𐑿 𐑜𐑱𐑥.\n" +
				"**!𐑕𐑑𐑪𐑐**, **!𐑧𐑯𐑛**, **!stop**, **!end**: 𐑧𐑯𐑛𐑟 𐑞 𐑜𐑱𐑥 𐑚𐑰𐑦𐑙 𐑐𐑤𐑱𐑛.\n" +
				"**!𐑜𐑧𐑕**, **!guess**: 𐑜𐑧𐑕 𐑩 𐑣𐑴𐑤 𐑢𐑻𐑛. (𐑯𐑴𐑑: 𐑦𐑓 𐑞 𐑸𐑜𐑘𐑩𐑥𐑩𐑯𐑑 𐑦𐑟 𐑴𐑯𐑤𐑦 𐑢𐑳𐑯 𐑤𐑧𐑑𐑼 𐑤𐑪𐑙, 𐑦𐑑𐑩𐑤 𐑜𐑧𐑕 𐑞𐑨𐑑 𐑤𐑧𐑑𐑼 𐑤𐑲𐑒 𐑦𐑑 𐑯𐑹𐑥𐑩𐑤𐑦 𐑢𐑫𐑛 𐑢𐑦𐑔𐑬𐑑 𐑞 𐑒𐑩𐑥𐑨𐑯𐑛.)\n" +
				"**!𐑣𐑨𐑙𐑥𐑨𐑯**, **!hangman**, 𐑕𐑧𐑑𐑕 𐑢𐑦𐑗 𐑗𐑨𐑯𐑩𐑤 𐑞 𐑚𐑪𐑑 𐑦𐑟 𐑦𐑯."
			)
		
		if game.status["playing"]:
			if command in ["𐑜𐑧𐑕", "guess"]:
				arg = " ".join(parts[1:])
				if len(arg) == 1 and arg in ALPHA:
					await game.guess_letter(channel, message.content)
				else:
					game.status["word_guesses"] += 1
					if arg == game.status["word"]:
						await game.send(channel, win=True)
					else:
						await game.send(channel)
			if len(message.content) == 1 and message.content in ALPHA:
				await game.guess_letter(channel, message.content)

client.run(config.token)
