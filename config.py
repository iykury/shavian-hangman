token = None
channel_id = None
mod_role_id = None

#Get token from file
try:
	with open("token") as f:
		token = f.read().strip("\n")
except:
	print("Couldn't get token")

#Get hangman channel id from file
try:
	with open("channel") as f:
		channel_id = int(f.read())
except:
	print("Couldn't get hangman channel id")

#Get moderator role id from file
try:
	with open("role") as f:
		mod_role_id = int(f.read())
except:
	print("Couldn't get moderator role id")
