#All Shavian letters
ALPHA = "𐑐𐑑𐑒𐑓𐑔𐑕𐑖𐑗𐑘𐑙𐑚𐑛𐑜𐑝𐑞𐑟𐑠𐑡𐑢𐑣𐑤𐑥𐑦𐑧𐑨𐑩𐑪𐑫𐑬𐑭𐑮𐑯𐑰𐑱𐑲𐑳𐑴𐑵𐑶𐑷𐑸𐑹𐑺𐑻𐑼𐑽𐑾𐑿"

TOTAL_GUESSES = 8

status = {}

def reset_status():
	global status
	status = {
		"playing": False,
		"word": None,
		"correct": [],
		"incorrect": [],
		"word_guesses": 0
	}

reset_status()

async def guess_letter(channel, letter):
	if letter in status["correct"] + status["incorrect"]:
		await channel.send(f"𐑿𐑝 𐑷𐑤𐑮𐑧𐑛𐑦 𐑜𐑧𐑕𐑑 ·{letter}")
	else:
		if letter in status["word"]:
			status["correct"].append(letter)
		else:
			status["incorrect"].append(letter)

		await send(channel)

async def send(channel, win=False, end=False):
	if status["playing"]:
		#U+2009 is a "THIN SPACE", so the characters are spaced out just a little bit more
		word = "\u2009".join(status["word"])
		actual_word = word #word could be modified later, so the original is kept if needed
		remaining = TOTAL_GUESSES - (len(status["incorrect"]) + status["word_guesses"])

		for char in ALPHA:
			if not char in status["correct"]:
				word = word.replace(char, "\\_")
		
		if remaining == 1:
			message = "1 𐑜𐑧𐑕 𐑮𐑦𐑥𐑱𐑯𐑦𐑙"
		else:
			message = str(remaining) + " 𐑜𐑧𐑕𐑩𐑟 𐑮𐑦𐑥𐑱𐑯𐑦𐑙"

		if status["incorrect"] != []:
			incorrect = " ".join(status["incorrect"])
			message = incorrect + "\n" + message
		
		if win or word == actual_word: #Win
			message += "\n" + actual_word + "\n𐑒𐑩𐑯𐑜𐑮𐑨𐑑𐑕, 𐑿 𐑢𐑳𐑯! :D"
			reset_status()
		elif remaining == 0: #Loss
			message += "\n" + actual_word + "\n𐑿 𐑤𐑪𐑕𐑑. :'("
			reset_status()
		elif end: #Manually end the game
			message += "\n" + actual_word + "\n𐑧𐑯𐑛𐑩𐑛 𐑞 𐑜𐑱𐑥."
			reset_status()
		else:
			message += "\n" + word
		
		await channel.send(message)
